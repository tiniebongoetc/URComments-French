// ==UserScript==
// @name           WME URComments Francais Belgique
// @description    Ce script est la traduction francaise des commentaires de URComments, il doit être utilisé avec le script URComments de RickZabel
// @namespace      tunisiano187@gmail.com
// @grant          none
// @grant          GM_info
// @version        0.0.7
// @match          https://editor-beta.waze.com/*editor*
// @match          https://beta.waze.com/*editor*
// @match          https://www.waze.com/*editor*
// @match          https://editor-beta.waze.com/*editor/*
// @match          https://beta.waze.com/*editor/*
// @match          https://www.waze.com/*editor/*
// @author         tunisiano187 '2018 based on Rick Zabel '2014
// @license        MIT/BSD/X11
// @updateURL      https://gitlab.com/WMEScripts/URComments-French/raw/master/%20URComments-(BEFR).user.js
// @compatible     chrome firefox
// @supportURL      mailto:incoming+WMEScripts/URComments-French@incoming.gitlab.com
// @contributionURL https://flattr.com/@tunisiano
// @icon			data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAAAwCAYAAACFUvPfAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDIxIDc5LjE1NTc3MiwgMjAxNC8wMS8xMy0xOTo0NDowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjQyQjZDNjdEODYzODExRTRBRDY0Q0I2QjA1MjU4N0EyIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjQyQjZDNjdFODYzODExRTRBRDY0Q0I2QjA1MjU4N0EyIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NDJCNkM2N0I4NjM4MTFFNEFENjRDQjZCMDUyNTg3QTIiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NDJCNkM2N0M4NjM4MTFFNEFENjRDQjZCMDUyNTg3QTIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6++Bk8AAANOElEQVR42tRZCWxU1xW9M39mPB5v431fMLYJdmpjthQUVsdlS9IQQkpIIDRhl5pKQUpbKkAEpakQIhVVRUytQIGwihCaBkgItQELQosxdrDZ7Njjbbx7vM0+f3ruZDz1NmTGhEj59tOb//979553313fl9jtdvqpXbLHRVgikTz0NbdJkyYJERERUp1OJ1Wr1WJLS4tYXFxswzu7s408+XFJ2g1oSUZGhtzf318piqLKx8dHZbPZFFKpVMC9TRAEs8lk0uNe39vbaywvL7eMBP5HAz179myZxWLxxfNg3IZHRkbG5OTkpEPSkQAs1Wq1nQUFBVXt7e2twNSGMdx3yuVyQ2FhofVHBw01kCsUigA8i1m9evXc3Nzc5TExMRMhUfnAOZC6VaPRlJ8+ffrzM2fOXMW9BvgazWZzD9TG8qOBZgnr9fqg5OTklPfff39bUlLSfL3ZKvmmqZ2q2rqoy2h2jAtSKmhsaBD9LDqUVAqZ/fbt29c2b978IfS9HCqjUalUXf0Sfyygp0+f7kB8584d6bhx4/xTU1PT9uzZk69WB2derdHSxQf1ZLTaRpyrlAmUkxpH05OiqbGxoWrjxo07Wltbb0KFNNevX+/FENEBmqUyWvCTJ0+WDPEKrh4S8oFXiDp+/HhedHT0M6fKvqWbDa0e0Z0YG05LMpPp/v37xWvXrn0XqlRWX1+vraysNEkfZu38zE1zXHPmzOH53ARuAQEBUuieBM2OJoaFhSl27NixAPr7TGFVo8eA+eKxPAc7Nen111/PgX5HxMXF+TIsmSe+1bkbEuintKamRoBeyqxWq6Knp0eA2xJAUAJ3Zce9+PTTT9tkMpkF7opgQEEwwjU6g4kKKhu83sWCynrKjg2jhQsXPrd///4L2Dkm0iv9PntiSUIF5JmZmSpMCsI2hwNMNBYSC4+QgLUkoE909vF4HoP3kVhY+Pz589Mh/czi+layiqLXoK2inXhuVFRUUlZWViIE45eSkiI8LCKyZAUAZbfki8sfxhA4bdq0+GXLluUmJCRMBqCxkHQY9E2BdxwY2iDtqtra2hsHDhy4jIVOYTqV8BIDr3ERakd/r0Xn9nf/9aBNx4YpmTlzZtrNmzcvBwUFuQXNIZaDgRJS84eDV8+bN2/cqlWr1rF+AqTMbDFRU72WdI29ZNZbSaGSKdQx/jFRcdExERGTZ6Snp/8GYbmGiXVBPQZeyyakOvrtX/7X7e/+S2f4ziXCPoIhaam73MMBGJcvBgXBP4bv3LnztSlTpmwAWOW9svtU/kkd1V/rINE23ONIBQnFTQuh1OciZXHJsSn8TBwy7NitB67g7O53/yX8386sHOqhgnbZSCrBEoaOqpVKZXReXt5W6OfC5uZGuvjnW9RU2v1QPbRZ7aS50kbVl5spY2kHLdg4i0L9lNRtMrvGDNx+d7/7rxCVj6Nva2vTArARPts21BClHR0dPqy7MKgIAOYItrD8ZgUdWXmFtCVdZIfYPGsILufqsBsipYYHjTpQpYWrCXjEixcv3oKX6oNXGgRasmDBAhkyMD+MCd21a9dKAF5QUVxB598uJZvR5nB9njZHcOm20oOva2lKfAT5yASvAXN0nIy5zc3NJRUVFd/CvvpY26QDsjABhqMEw0AYXQZ0eG1TUwOd+30pr9QrwA7Q+JfapVT0j1sE46BF4xO9Bv1sehIDF8+ePfsR7KmF01UOG/06LUGIFIKDg33hwtRvvPHGagzyOf9uMVlNVrdEx+ZEUdZLSZSYlkBymYK6ejrp/rVqupFfTT3NBodNNd1pp6IjJTRzxSRHcsR5hyfXL9LiaWJcOOcvJ/Pz8wvgSjud+bXLe0iR3yogIb+JEyeOiY+Pn1VRUkHaMt3I5Y5CSs/unkTjJ4wf9FwdGEJT54VQ1px0Or21kKqLWhGdZHRpXwn5h6goZ9F4ig5UEecgBsvIwghVKSHhRPjsYIIgv3jrrbfeMxqNWrhQA0DbXaChGhKkjwpI2W/JkiXsh4XS4xq3SdSczRnDAA+8fBS+9OKOuZS/4jPS1fUhlRTo0z8VUGeHjua+Ng3pp47+U9viGv8Egkp0oB+NCQlEehrI6mhEarpvw4YNfzMYDM3IEntPnjxpG1QjsmogPCtgnX6JiYnZJrPRISW7OBy0b4Ccsudkfu/2KuQ+NGXtGPrij9+QiD8b/vyDVWSDfVQ0dTrGBPjI6YUnk+mJyGDOF+wACCj1Xx47duwQ9Pge7ruReJmcdePgwjY8PFzKtRoinxKpZFJjbSNXESOCCc8IIgQdj/QyeUI8AkupA3DChCiaujCTyps7KF7tT2mQ7oSYMJJJyFp840beoUOHjiBM17OHAG8DUgTzgCJ3eDXOKSUsU4ZtUSDHUHc0drlVjYAYpcfWLyBL6KczY/kkkkgl9CQqE27skZAb30Cuve/ChQuFiA9aCM9YVFRke1gl7gKN1UkQtlnaUq7bLMglyA3omGzPA0VjdZODDjJwOrXlIl3PKiOFv5ySc8IoKT2BkMt8AL4VXMjCyPq+D+ywcw+AtbNKoFnkKplctItDPIZArx6cRWOSx3oMuvhgFfXTsejtVH2tyZHspuZGENwru68upAt9UDeLp4DJWXUQJyFI6kVMtvX19XWExquHBQsL/PX9As8T+Suffk0PLjcOCjZkl3CFR5Fjwnh3O2BDlv4kyJvA5QDNFYczizK3t7fXxMbHkVQhcUhpYCvaW0H7Vp+iqsoHDwX87xNF9MWOkmHzuTHdmLg4gg5XMz/m6+RPXkkamZOIbeItMty7d++WXCan1LnRHOaHtbpbzVT4QZljxTbRRof/8E/au+oEHd3+LxewygtNI87llga6TP/u3bulzI/5Mn+vz/JQMNpQdXCmrj948GBRbm7uqqmvjfOpOKsZcdK317T0l5c/JptJpM7671LV+jJCFvixw0O01ejcV++vphFU0XT48OEi2I+e8yrm77WkCwsLRURDM3S6j8t0RKPC1CfSaOysGLd61VrZSR11XYOetWl01Frd6XYO00sbP47gKQpRkmmZH/Nl/l6DZhMBWOT+FnY7nbt37z4Bwfcs3jaLfIOUXmd4IzWmw/SYLtNnPsyP+XrjOQaBhqO3wmfqwUBXVVVVjVj/kTooxL48fzYJPsKIRuVp4/lMh+kxXabPfJgf8x0taEcph2TbzPEev1v27t174dKlS6fGpqTSm0fnU0C4alQS5nk8n+mA3idMl+kzH+bntFAaLWiWNm+VHv6zHX3D1q1bD3/11VcnksYki7898yvKfGkMOHgGlsdlvphMPI/nMx3QO8R0nfT1Tn5en8e5zvIGFrZc6fDBDIhHwJfGvvLKK7NXrFjxa+QoIVptA109WUqlJ2uot1M/jKBcIaOpq9Jo+tIsio6O5RjQgWToo6NHj15C1G2AHrfA+PggxAgDdOUZ3pwlDgU9CDhcUgDcUxisPDIkJCQBCflzTz311BzUkUG1dTX01+c/Iat5sLd6YefPadaiGQy2+/r16wV79uz5rLOzUwNazdDhNtDqGQr4hwDtAg7GCpVK5YeQq4bUQyCpSDCOfeedd55HHTm/8MwV+nTzVdekJ+cn0Zu7XubsrWLNmjUfYpfq0Jqw8HaEah0KjT5OOYcC/qFAu87xAF6u0+mU2FJ/gOZTnkg8jz9w4MCm5OTkjL+/fYxun9eQOiqAfvf5ShQOEt26deve1Wg0d0FbC3VoR+tBns7StTgNzz7SIedoDJFGOGfmbbYhxzZBWj0A3c6SQ2vYtm1bPpKrruXvLSJ1tD+9ujeHfJV+Yl5e3n4EjkoGDJVoY8A8f0ColgykP6qvDCPp9NKlS6UlJSUyqIYMDAU+u8MYmfNLlD+kHQbgcYsXL56xadOm9XpDr9RPFUAFBQVfbtmy5Qho1rFb4zVjjhH31sDAQCvcHJ+7WLu7u22IitaBn94eRT1cugxg/CXKl8/vMEbOF/d8tIBxfIIaivvI7du3/zInJ2d2XV1dzcqVKz+EZDlb4tPzHrw3YryZQXNihN0y8yIw1xAREWE8d+5cv7o8EmhpSkqKHGWPH0Cr+XiMz4TZk3Apxh6tHziYx+J3KNYSCA+xaOfOnVeqq6ubQUuH941o7NYYlJULC4w14Z0ehtyLe37XY8SFOtD6HWa7d1newEVwkcuqwODQs5T5k4EvepY+PxMgMTkWwc9l4Gtfv379ebwX0QS89+HzE/Qc7fhs28qVCcYL/LUAcy0Od65QCJj7g3xmtrPBREVFOXJrMOdi1wYAnLbKISHWbWbOC+vg+XzPjZUV4/mrq5V7bpC2o7jghnszABv4EJH9NPhY+w9fHhl0dna2FQQNXE1gK01wdQpIhMexWjgAcyXt7LmxivEnGTvXmUyDF8D3zm13nCszcNZrVhN4HRaC2Z37G5X36P/YjtJLCA0NlfIRA38UQi+BtCT8Ycj5hVUy/NhAcIFgb8H3SqVSZCH4+fmJ7DmgguLjiIhDvwmyG+SyTALmHvtYLNIOcHaei5S0H5X9UYPL/wQYAOwQASZqvrLnAAAAAElFTkSuQmCC
// ==/UserScript==

/* Changelog
 * 0.0.1 - Version initiale
 * 0.0.2 - Version alpha
 * 0.0.3 - ajout d'Erreur générales
 * 0.0.4 - Import de la signature depuis la base de données du navigateur
 * 0.0.5 - Explications de la mise à jour de la signature
 * 0.0.6 - Clôture sans réponses
 * 0.0.7 - Inclusion du PS
 */
//Ce script est le complément français orienté pour la belgique. Si nécessaire, n'hésitez pas à me contacter pour faire une autre version.
//Si vous souhaitez inclure des quotes dans vos titres ou commentaires, vous devez les \"échaper" exemple "Comment \"Comment\" Comment",
//Si vous souhaitez faire du multiligne, vous avez deux possibilités, utiliser \n. example "Line1\nLine2",
//ou \r\ en fin de ligne et vous continuez à la ligne (cette technique ne fonctionne pas sur les anciens navigateurs mais est plus propre"
//Si vous souhaitez insérer une ligne vide, utilisez \n\n. example "Line1\n\nLine2",
//
// Afin de ne pas charger l'affichage, je ne change pas les textes qui n'apparaissent que pour l'éditeur.
//

// Configuration de la signature
var URCommentCustomSignatures = localStorage.getItem('Signature'); // Vous pouvez placer ici votre utilisateur, votre niveau ainsi que les informations de zone, pour 
// une signature multi-ligne, utilisez \r\ avant de passer à la ligne
if (URCommentCustomSignatures !== "") {
    var URCommentCustomSignature = "\r\
" + URCommentCustomSignatures;
}

//Configuration des post scriptum
        var URCommentCustompostscriptum = "";
        var showPS = localStorage.getItem('URCommentsShowPS');
        if (showPS === null) {
            showPS = true;
        }
        if(showPS) {
            URCommentCustompostscriptum = localStorage.getItem('URCommentsShowPS');
            if(URCommentCustompostscriptum === "")
            {
                URCommentCustompostscriptum = "PS : Waze repose sur une participation bénévole de rapporteurs et d’éditeurs. \r\
Notez que si vous le souhaitez, vous pouvez éditer personnellement la carte et rejoindre la communauté des éditeurs sur notre plate-forme de communication collaborative : http://wazebelgium.be/join-slack/"; // Variable PS qui sera inclue dans certaines signatures
            }
        } 
        
if (URCommentCustompostscriptum !== "") {
    var URCommentCustomPS = "\r\
" + URCommentCustompostscriptum;
}

var URCommentCustomVersion = GM_info.script.version;
var URCommentCustomUpdateMessage = "yes"; // yes alert the user, no has a silent update.
var URCommentCustomVersionUpdateNotes = "Une nouvelle version du fichier de commentaires a été installée, n'oubliez pas de recopier vos modifications dans le script ! v" + URCommentCustomVersion;

if (URCommentCustomUpdateMessage === "yes") {
	if (localStorage.getItem('URCommentCustomVersion') !== URCommentCustomVersion) {
		alert(URCommentCustomVersionUpdateNotes);
		localStorage.setItem('URCommentCustomVersion', URCommentCustomVersion);
	}
}

//Configuration personalisée : ceci permet l'automatisation de vos messages de rappel, ainsi que vos fermeture en "non identifiée" d'être nommées comme vous le souhaitez.
//La position de ces messages est importante, les positions se comtent avec les "," en commencant à 0.
//On compte donc les titres, commentaires, et statut de la demande. Dans cette liste, le rappel suivant est "Rappel"
window.UrcommentsCustomReminderPosistion = 27;

//Ceci est le message qui est ajouté à l'option "lien de rappel"(reminder link)
window.UrcommentsCustomReplyInstructions = "Pour répondre, veuillez utiliser l'application Waze ou vous rendre à l'adresse suivante "; //suivi par l'URL - tunisiano187 10/7/2018

//La position du message "fermer en "non identifié" (compte identique au rappel). Dans cette liste, le titre est "Aucune réponses"
window.UrcommentsCustomCloseNotIdentifiedPosistion = 30;

//Ceci est la liste des type d'UR par défaut de waze. Editez cette liste en tenant compte des termes utilisés dans votre zone!
//Afin que celà fonctionne, ces message doivent correspondre à vos titres de commentaires automatiques!
window.UrcommentsCustomdef_names = [];
window.UrcommentsCustomdef_names[6] = "Incorrect turn"; //"Incorrect turn"; A modifier
window.UrcommentsCustomdef_names[7] = "Adresse incorrecte"; //"Incorrect address";
window.UrcommentsCustomdef_names[8] = "Route incorrecte"; //"Incorrect route" A vérifier;
window.UrcommentsCustomdef_names[9] = "Rond-point manquant"; //"Missing roundabout" A vérifier;
window.UrcommentsCustomdef_names[10] = "Erreur générale"; //"General error";
window.UrcommentsCustomdef_names[11] = "Interdiction de tourner"; //"Turn not allowed";
window.UrcommentsCustomdef_names[12] = "Jonction incorrecte"; //"Incorrect junction";
window.UrcommentsCustomdef_names[13] = "Pont surélevé manquant"; //"Missing bridge overpass" A vérifier;
window.UrcommentsCustomdef_names[14] = "Mauvais sens de circulation"; //"Wrong driving direction"; A vérifier
window.UrcommentsCustomdef_names[15] = "Sortie manquante"; //"Missing Exit";
window.UrcommentsCustomdef_names[16] = "Route manquante"; //"Missing Road";
window.UrcommentsCustomdef_names[18] = "Point d'intérêt manquant"; //"Missing Landmark" A vérifier;
window.UrcommentsCustomdef_names[19] = "Route bloquée"; //"Blocked Road" A vérifier;
window.UrcommentsCustomdef_names[21] = "Nom de rue manquante"; //"Missing Street Name" A vérifier;
window.UrcommentsCustomdef_names[22] = "Préfixe/Suffixe de rue incorrect"; //"Incorrect Street Prefix or Suffix" A vérifier;
window.UrcommentsCustomdef_names[23] = "Mauvaise limitation de vitesse"; //"Missing or invalid speed limit"; A vérifier


//below is all of the text that is displayed to the user while using the script
window.UrcommentsCustomURC_Text = [];
window.UrcommentsCustomURC_Text_tooltip = [];
window.UrcommentsCustomURC_USER_PROMPT = [];
window.UrcommentsCustomURC_URL = [];

//zoom out links
window.UrcommentsCustomURC_Text[0] = "Zoom Out 0 & Close UR";
window.UrcommentsCustomURC_Text_tooltip[0] = "Zooms all the way out and closes the UR window";

window.UrcommentsCustomURC_Text[1] = "Zoom Out 2 & Close UR";
window.UrcommentsCustomURC_Text_tooltip[1] = "Zooms out to level 2 and closes the UR window (this is where I found most of the toolbox highlighting works)";

window.UrcommentsCustomURC_Text[2] = "Zoom Out 3 & Close UR";
window.UrcommentsCustomURC_Text_tooltip[2] = "Zooms out to level 3 and closes the UR window (this is where I found most of the toolbox highlighting works)";

window.UrcommentsCustomURC_Text_tooltip[3] = "Reload the map";

window.UrcommentsCustomURC_Text_tooltip[4] = "Number of URs Shown";

//tab names
window.UrcommentsCustomURC_Text[5] = "Commentaires"; //"Comments";
window.UrcommentsCustomURC_Text[6] = "Filtrage des URs"; //"UR Filtering";
window.UrcommentsCustomURC_Text[7] = "Paramètres"; //"Settings";

//UR Filtering Tab
window.UrcommentsCustomURC_Text[8] = "Cliquez ici pour plus d'informations"; //"Click here for Instructions";
window.UrcommentsCustomURC_Text_tooltip[8] = "Instructions pour le filtrage des URs"; //"Instructions for UR filtering";
window.UrcommentsCustomURC_URL[8] = "https://docs.google.com/presentation/d/1zwdKAejRbnkUll5YBfFNrlI2I3Owmb5XDIbRAf47TVU/edit#slide=id.p";


window.UrcommentsCustomURC_Text[9] = "Activer le filtrage d'URs (URComments)"; //"Enable URComments UR filtering";
window.UrcommentsCustomURC_Text_tooltip[9] = "Activer ou désactiver le filtrage (URComments)"; //"Enable or disable URComments filtering";

window.UrcommentsCustomURC_Text[10] = "Activer le compteur d'URs"; //"Enable UR pill counts";
window.UrcommentsCustomURC_Text_tooltip[10] = "Enable or disable the pill with UR counts";

window.UrcommentsCustomURC_Text[12] = "Masquer l'attente"; //"Hide Waiting";
window.UrcommentsCustomURC_Text_tooltip[12] = "Montrer seulement les URs à traîter (cacher les états intermédiaires)"; //"Only show URs that need work (hide in-between states)";

window.UrcommentsCustomURC_Text[13] = "Montrer seulement mes URs"; //"Only show my URs";
window.UrcommentsCustomURC_Text_tooltip[13] = "Hide URs where you have no comments";

window.UrcommentsCustomURC_Text[14] = "Montrer aux autres le rappel passé de l'UR et le fermer"; //"Show others URs past reminder + close";
window.UrcommentsCustomURC_Text_tooltip[14] = "Show URs that other commented on that have gone past the reminder and close day settings added together"; //"Show URs that other commented on that have gone past the reminder and close day settings added together";

window.UrcommentsCustomURC_Text[15] = "Masquer le rappel des URs nécessaires"; //"Hide URs Reminder needed";
window.UrcommentsCustomURC_Text_tooltip[15] = "Masquer les URs où des rappels sont nécessaires"; //"Hide URs where reminders are needed";

window.UrcommentsCustomURC_Text[16] = "Hide URs user replies";
window.UrcommentsCustomURC_Text_tooltip[16] = "Hide UR with user replies";

window.UrcommentsCustomURC_Text[17] = "Hide URs close needed";
window.UrcommentsCustomURC_Text_tooltip[17] = "Hide URs that need closing";

window.UrcommentsCustomURC_Text[18] = "Hide URs no comments";
window.UrcommentsCustomURC_Text_tooltip[18] = "Hide URs that have zero comments";

window.UrcommentsCustomURC_Text[19] = "hide 0 comments without descriptions";
window.UrcommentsCustomURC_Text_tooltip[19] = "Hide URs that do not have descriptions or comments";

window.UrcommentsCustomURC_Text[20] = "hide 0 comments with descriptions";
window.UrcommentsCustomURC_Text_tooltip[20] = "Hide URs that have descriptions and zero comments";

window.UrcommentsCustomURC_Text[21] = "Hide Closed URs";
window.UrcommentsCustomURC_Text_tooltip[21] = "Hide closed URs";

window.UrcommentsCustomURC_Text[22] = "Hide Tagged URs";
window.UrcommentsCustomURC_Text_tooltip[22] = "Hide URs that are tagged with URO+ style tags ex. [NOTE]";

window.UrcommentsCustomURC_Text[23] = "Reminder days: ";

window.UrcommentsCustomURC_Text[24] = "Close days: ";

//settings tab
window.UrcommentsCustomURC_Text[25] = "Auto set new UR comment";
window.UrcommentsCustomURC_Text_tooltip[25] = "Auto set the UR comment on new URs that do not already have comments";

window.UrcommentsCustomURC_Text[26] = "Auto set reminder UR comment";
window.UrcommentsCustomURC_Text_tooltip[26] = "Auto set the UR reminder comment for URs that are older than reminder days setting and have only one comment";

window.UrcommentsCustomURC_Text[27] = "Auto zoom in on new UR";
window.UrcommentsCustomURC_Text_tooltip[27] = "Auto zoom in when opening URs with no comments and when sending UR reminders";

window.UrcommentsCustomURC_Text[28] = "Auto center on UR";
window.UrcommentsCustomURC_Text_tooltip[28] = "Auto Center the map at the current map zoom when UR has comments and the zoom is less than 3";

window.UrcommentsCustomURC_Text[29] = "Auto click open, solved, not identified";
window.UrcommentsCustomURC_Text_tooltip[29] = "Suppress the message about recent pending questions to the reporter and then depending on the choice set for that comment Clicks Open, Solved, Not Identified";

window.UrcommentsCustomURC_Text[30] = "Auto save after a solved or not identified comment";
window.UrcommentsCustomURC_Text_tooltip[30] = "If Auto Click Open, Solved, Not Identified is also checked, this option will click the save button after clicking on a UR-Comment and then the send button";

window.UrcommentsCustomURC_Text[31] = "Auto close comment window";
window.UrcommentsCustomURC_Text_tooltip[31] = "For the user requests that do not require saving this will close the user request after clicking on a UR-Comment and then the send button";

window.UrcommentsCustomURC_Text[32] = "Auto reload map after comment";
window.UrcommentsCustomURC_Text_tooltip[32] = "Reloads the map after clicking on a UR-Comment and then send button. This does not apply to any messages that needs to be saved, since saving automatically reloads the map. Currently this is not needed but I am leaving it in encase Waze makes changes";

window.UrcommentsCustomURC_Text[33] = "Auto zoom out after comment";
window.UrcommentsCustomURC_Text_tooltip[33] = "After clicking on a UR-Comment in the list and clicking send on the UR the map zoom will be set back to your previous zoom";

window.UrcommentsCustomURC_Text[34] = "Auto switch to the UrComments tab";
window.UrcommentsCustomURC_Text_tooltip[34] = "Auto switch to the URComments tab when opening a UR, when the UR window is closed you will be switched to your previous tab";

window.UrcommentsCustomURC_Text[35] = "Close message - double click link (auto send)";
window.UrcommentsCustomURC_Text_tooltip[35] = "Add an extra link to the close comment when double clicked will auto send the comment to the UR windows and click send, and then will launch all of the other options that are enabled";

window.UrcommentsCustomURC_Text[36] = "All comments - double click link (auto send)";
window.UrcommentsCustomURC_Text_tooltip[36] = "Add an extra link to each comment in the list that when double clicked will auto send the comment to the UR windows and click send, and then will launch all of the other options that are enabled";

window.UrcommentsCustomURC_Text[37] = "Comment List";
window.UrcommentsCustomURC_Text_tooltip[37] = "This shows the selected comment list. There is support for a custom list. If you would like your comment list built into this script or have suggestions on the Comments team’s list, please contact me at rickzabel @waze or @gmail";

window.UrcommentsCustomURC_Text[38] = "Disable done / next buttons";
window.UrcommentsCustomURC_Text_tooltip[38] = "Disable the done / next buttons at the bottom of the new UR window";

window.UrcommentsCustomURC_Text[39] = "Unfollow UR after send";
window.UrcommentsCustomURC_Text_tooltip[39] = "Unfollow UR after sending comment";

window.UrcommentsCustomURC_Text[40] = "Auto send reminders";
window.UrcommentsCustomURC_Text_tooltip[40] = "Auto send reminders to my UR as they are on screen";

window.UrcommentsCustomURC_Text[41] = "Replace tag name with editor name";
window.UrcommentsCustomURC_Text_tooltip[41] = "When a UR has the logged in editors name in the description or any of the comments of the UR (not the name Waze automatically add when commenting) replace the tag type with the editors name";

window.UrcommentsCustomURC_Text[42] = "(Double Click)"; //double click to close links
window.UrcommentsCustomURC_Text_tooltip[42] = "Double click here to auto send - ";

window.UrcommentsCustomURC_Text[43] = "Dont show tag name on pill";
window.UrcommentsCustomURC_Text_tooltip[43] = "Dont show tag name on pill where there is a URO tag";

window.UrcommentsCustomURC_Text[44] = "Signature";
window.UrcommentsCustomURC_Text_tooltip[44] = "Here you can set your signature, after making a change, please refresh your page to update the texts";

window.UrcommentsCustomURC_USER_PROMPT[0] = "UR Comments - Vous avez une ancienne version, ou votre fichier est corrompu du fichier de traduction, une erreur de synthaxe peut également être à la base de l'erreur. Missing: ";

window.UrcommentsCustomURC_USER_PROMPT[1] = "UR Comments - You are missing the following items from your custom comment list: ";

window.UrcommentsCustomURC_USER_PROMPT[2] = "List can not be found you can find the list and instructions at https://wiki.waze.com/wiki/User:Rickzabel/UrComments/";

window.UrcommentsCustomURC_USER_PROMPT[3] = "URComments - Vous ne pouvez pas régler la fermeture à zéro jours";

window.UrcommentsCustomURC_USER_PROMPT[4] = "URComments - To use the double click links you must have the Auto click open, solved, not identified option enabled";

window.UrcommentsCustomURC_USER_PROMPT[5] = "URComments - Aborting FilterURs2 because both filtering, counts, and auto reminders are disabled";

window.UrcommentsCustomURC_USER_PROMPT[6] = "URComments: Le chargement des informations de l'UR à expiré, nouvelle tentative."; //this message is shown across the top of the map in a orange box, length must be kept short

window.UrcommentsCustomURC_USER_PROMPT[7] = "URComments: Ajout du message de rappel à l'UR: "; //this message is shown across the top of the map in a orange box, length must be kept short

window.UrcommentsCustomURC_USER_PROMPT[8] = "URComment's UR Filtering has been disabled because URO+\'s UR filters are active."; //this message is shown across the top of the map in a orange box, length must be kept short

window.UrcommentsCustomURC_USER_PROMPT[9] = "UrComments has detected that you have unsaved edits!\n\nWith the Auto Save option enabled and with unsaved edits you cannot send comments that would require the script to save. Please save your edits and then re-click the comment you wish to send.";

window.UrcommentsCustomURC_USER_PROMPT[10] = "URComments: Can not find the comment box! In order for this script to work you need to have a UR open."; //this message is shown across the top of the map in a orange box, length must be kept short

window.UrcommentsCustomURC_USER_PROMPT[11] = "URComments - This will send reminders at the reminder days setting. This only happens when they are in your visible area. NOTE: when using this feature you should not leave any UR open unless you had a question that needed an answer from the wazer as this script will send those reminders."; //conformation message/ question


//Le format des commentaires doit correspondre à ceci,
// "Titre",     * ce que vous verrez dans la liste des messages dans WME (doit correspondre aux textes des types d'UR Waze par défaut)
// "comment",   * ce qui sera envoyé au Wazer
// "URStatus"   * cette section correspond à l'état de votre UR après le click "Ouvert, Résolu, Non identifié". après avoir clisué sur envoyer, létat sera mis automatiquement. les possibilités sont. "Open", "Solved",ou "NotIdentified",
// Si vous souhaitez laisser une ligne vide entre les commentaires (menu) entrez les lignes suivantes
// "<br>",
// "",
// "",

//Custom list
window.UrcommentsCustomArray2 = [
//1 (0) - Corrigé
    "Corrigé",
    "Merci beaucoup pour votre collaboration ! 🙂\r\
\r\
Le problème relaté est à présent corrigé. La modification sera effective dès la prochaine mise à jour de la carte qui devrait avoir lieu d’ici plus ou moins 48 heures.\r\
\r\
Grâce à votre participation, le réseau routier peut sans cesse être mis à jour en temps réel et permettre un guidage toujours plus performant.\r\
\r\
N’hésitez jamais à nous signaler vos remarques et toutes les erreurs que vous pourriez rencontrer sur vos trajets :\r\
\r\
    Route barrée (travaux).\r\
    Mauvaise signalisation de limitation de vitesse.\r\
    Changement de sens de circulation.\r\
    Interdiction de tourner.\r\
    Route manquante.\r\
    Etc.\r\
\r\
Ensemble, tentons continuellement de rentre l’application toujours plus agréable et la plus fonctionnelle possible !\r\
\r\
Bonne journée et bonne route avec Waze ! 🙂\r\
" + URCommentCustomSignature,
    "Solved", // tunisiano187 10/7/18

// 2 (3)
    "<br>",
    "",
    "",

// 3 (6)
    "Vous avez signalé", //7
    "Bonjour, \r\
\r\
Merci pour votre signalement. \r\
\r\
Vous avez signalé \r\
, pourriez-vous développer plus précisément le problème que vous avez rencontré afin que les modifications nécessaires puissent être apportées ?\r\
\r\
N’hésitez pas à vous aider de la carte afin d’énoncer le plus clairement possible le problème rencontré : https://www.waze.com/fr/livemap (Nom de rues, estimation de la durée des travaux, etc.)\r\
\r\
Merci pour votre contribution à l’amélioration de la carte.\r\
\r\
Bonne route avec Waze ! 🙂\r\
\r\
PS : Waze repose sur une participation bénévole de rapporteurs et d’éditeurs.\r\
Notez que si vous le souhaitez, vous pouvez éditer personnellement la carte et rejoindre la communauté des éditeurs sur notre plate-forme de communication collaborative : http://wazebelgium.be/join-slack/ \r\
" + URCommentCustomSignature,
    "Open",

// 3 (9)
    "Adresse incorrecte", //7
    "Bonjour, \r\
\r\
Merci pour votre signalement. \r\
\r\
Vous avez signalé une adresse incorrecte, par soucis de confidentialité, Waze ne nous communique ni votre point de départ, ni l'arrivée.\r\
Afin de résoudre votre soucis, pourriez-vous développer plus précisément le problème que vous avez rencontré afin que les modifications nécessaires puissent être apportées ?\r\
Quelle est l'adresse incorrecte que vous souhaitez signaler?\r\
L'emplacement de votre signalement est'il l'emplacement exact de l'adresse à corriger ?\r\
N’hésitez pas à vous aider de la carte afin d’énoncer le plus clairement possible le problème rencontré : https://www.waze.com/fr/livemap (Nom de rues, estimation de la durée des travaux, etc.)\r\
\r\
Merci pour votre contribution à l’amélioration de la carte.\r\
\r\
Bonne route avec Waze ! 🙂\r\
\r\
PS : Waze repose sur une participation bénévole de rapporteurs et d’éditeurs.\r\
Notez que si vous le souhaitez, vous pouvez éditer personnellement la carte et rejoindre la communauté des éditeurs sur notre plate-forme de communication collaborative : http://wazebelgium.be/join-slack/ \r\
" + URCommentCustomSignature,
    "Open",

// 4 (12)
    "Route manquante", //7
    "Bonjour, \r\
\r\
Merci pour votre signalement. \r\
\r\
Vous avez signalé une route manquante, pourriez-vous développer plus précisément le problème que vous avez rencontré afin que les modifications nécessaires puissent être apportées ?\r\
\r\
N’hésitez pas à vous aider de la carte afin d’énoncer le plus clairement possible le problème rencontré : https://www.waze.com/fr/livemap (Nom de rues, estimation de la durée des travaux, etc.)\r\
\r\
Merci pour votre contribution à l’amélioration de la carte.\r\
\r\
Bonne route avec Waze ! 🙂\r\
\r\
PS : Waze repose sur une participation bénévole de rapporteurs et d’éditeurs.\r\
Notez que si vous le souhaitez, vous pouvez éditer personnellement la carte et rejoindre la communauté des éditeurs sur notre plate-forme de communication collaborative : http://wazebelgium.be/join-slack/ \r\
" + URCommentCustomSignature,
    "Open",

// 5 (15)
    "Jonction incorrecte", //7
    "Bonjour, \r\
\r\
Merci pour votre signalement. \r\
\r\
Vous avez signalé une jonction incorrecte, pourriez-vous développer plus précisément le problème que vous avez rencontré afin que les modifications nécessaires puissent être apportées ?\r\
\r\
N’hésitez pas à vous aider de la carte afin d’énoncer le plus clairement possible le problème rencontré : https://www.waze.com/fr/livemap (Nom de rues, estimation de la durée des travaux, etc.)\r\
\r\
Merci pour votre contribution à l’amélioration de la carte.\r\
\r\
Bonne route avec Waze ! 🙂\r\
\r\
PS : Waze repose sur une participation bénévole de rapporteurs et d’éditeurs.\r\
Notez que si vous le souhaitez, vous pouvez éditer personnellement la carte et rejoindre la communauté des éditeurs sur notre plate-forme de communication collaborative : http://wazebelgium.be/join-slack/ \r\
" + URCommentCustomSignature,
    "Open",

// 6 (18)
    "Interdiction de tourner", //7
    "Bonjour, \r\
\r\
Merci pour votre signalement. \r\
\r\
Vous avez signalé une interdiction de tourner, pourriez-vous développer plus précisément le problème que vous avez rencontré afin que les modifications nécessaires puissent être apportées ?\r\
Est-ce dû à des travaux, route en sens unique, etc. ? \r\
\r\
N’hésitez pas à vous aider de la carte afin d’énoncer le plus clairement possible le problème rencontré : https://www.waze.com/fr/livemap (Nom de rues, estimation de la durée des travaux, etc.)\r\
\r\
Merci pour votre contribution à l’amélioration de la carte.\r\
\r\
Bonne route avec Waze ! 🙂\r\
\r\
PS : Waze repose sur une participation bénévole de rapporteurs et d’éditeurs.\r\
Notez que si vous le souhaitez, vous pouvez éditer personnellement la carte et rejoindre la communauté des éditeurs sur notre plate-forme de communication collaborative : http://wazebelgium.be/join-slack/ \r\
" + URCommentCustomSignature,
    "Open",

// 7 (21)
    "Mauvais sens de circulation", //8
    "Bonjour, \r\
\r\
Merci pour votre signalement. \r\
\r\
Vous avez signalé un mauvais sens de circulation, pourriez-vous développer plus précisément le problème que vous avez rencontré afin que les modifications nécessaires puissent être apportées ?\r\
Est-ce permanent ? route concernée, ...? \r\
\r\
N’hésitez pas à vous aider de la carte afin d’énoncer le plus clairement possible le problème rencontré : https://www.waze.com/fr/livemap (Nom de rues, estimation de la durée des travaux, etc.)\r\
\r\
Merci pour votre contribution à l’amélioration de la carte.\r\
\r\
Bonne route avec Waze ! 🙂\r\
\r\
PS : Waze repose sur une participation bénévole de rapporteurs et d’éditeurs.\r\
Notez que si vous le souhaitez, vous pouvez éditer personnellement la carte et rejoindre la communauté des éditeurs sur notre plate-forme de communication collaborative : http://wazebelgium.be/join-slack/ \r\
" + URCommentCustomSignature,
    "Open",

// 8 (24)
    "Protection d'adresse",
    "Pour des raisons de confidentialité, Waze ne nous communique pas vos points de départ et d'arrivée. Afin de régler votre soucis, pourriez-vous nous communiquer l'adresse de destination? Merci! \r\
    " + URCommentCustomSignature, 
    "Open",

// 9 (27)
    "Rappel",
    "Bonjour,\r\
\r\
PETIT RAPPEL : Sans réponse de votre part, nous nous verrons contraint de clôturer ce problème sans avoir pu effectuer les corrections nécessaires ; ce qui serait fort dommage !\r\
\r\
Si d’aventure, ce signalement avait été la conséquence d’une fausse manœuvre de votre part, veuillez juste nous le signaler ici même afin que nous puissions fermer cette boîte de discussion. La communauté ne vous en voudra pas ! 😉\r\
Merci pour votre collaboration.\r\
" + URCommentCustomSignature,
    "Open",

// 10 (30)
    "Cloture sans réponses",
    "Bonjour, \r\
\r\
N'ayant pas de réponse de votre part, nous allons clôturer cette demande.\r\
\r\
N’hésitez jamais à nous signaler vos remarques et toutes les erreurs que vous pourriez rencontrer sur vos trajets :\r\
\r\
    Route barrée (travaux).\r\
    Mauvaise signalisation de limitation de vitesse.\r\
    Changement de sens de circulation.\r\
    Interdiction de tourner.\r\
    Route manquante.\r\
    Etc.\r\
\r\
Ensemble, tentons continuellement de rentre l’application toujours plus agréable et la plus fonctionnelle possible !\r\
\r\
Bonne route avec Waze ! 🙂" + URCommentCustomSignature,
    "NotIdentified",


// 11 (33)
    "<br>",
    "",
    "",


// 12 (36)
    "Erreur générale",
    "Bonjour, \r\
\r\
Merci pour votre signalement. \r\
\r\
Vous avez signalé une erreur générale, pourriez-vous développer plus précisément le problème que vous avez rencontré afin que les modifications nécessaires puissent être apportées ?\r\
\r\
N’hésitez pas à vous aider de la carte afin d’énoncer le plus clairement possible le problème rencontré : https://www.waze.com/fr/livemap (Nom de rues, estimation de la durée des travaux, etc.)\r\
\r\
Merci pour votre contribution à l’amélioration de la carte.\r\
\r\
Bonne route avec Waze ! 🙂\r\
\r\
PS : Waze repose sur une participation bénévole de rapporteurs et d’éditeurs.\r\
Notez que si vous le souhaitez, vous pouvez éditer personnellement la carte et rejoindre la communauté des éditeurs sur notre plate-forme de communication collaborative : http://wazebelgium.be/join-slack/ \r\
" + URCommentCustomSignature,
    "Open",

// last
    "<br>",
    "",
    ""
// Les messages non traduits sont déplacés dans le fichier atraduire.txt
];
//end Custom list
